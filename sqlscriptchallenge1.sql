use challenges;
DELIMITER //

Create procedure sp_wavp(starttradetime varchar(20))
begin

set @starttradetime=starttradetime;

select
sum(vol*close)/sum(vol) as WAVP,
date_Format(str_to_date(@starttradetime, "%Y%m%d%H%i"), "%d/%m/%Y") as `date`,
date_format(str_to_date(@starttradetime, "%Y%m%d%H%i"), "%H:%i") as `start`,
date_format(date_add(str_to_date(@starttradetime, "%Y%m%d%H%i"), INTERVAL 5 HOUR), "%H:%i") as `end`
from sd2
where date between @starttradetime and date_format(date_add(str_to_date(@starttradetime, "%Y%m%d%H%i"), INTERVAL 5 HOUR), "%Y%m%d%H%i");

end //
DELIMITER ;

call sp_wavp('201010111000');
drop procedure sp_wavp;
