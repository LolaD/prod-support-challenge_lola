# SQL Challenge


**Challenge 1**

* Sample_dataset2 was uploaded within the 'challenges' database

* To use database: ``use challenges;``
* __Assumptions made__: Trade price found by using **'close price'** in the table
* A new table called __sd2__ was created. To execute this:

```
create table sd2 select
`<ticker>` as ticker,
`<date>` as date,
`<open>` as open,
`<high>` as high,
`<low>` as low,
`<close>` as close,
`<vol>` as vol
from sample_dataset2;
```
* To execute stored procedure:
```call sp_wavp('201010111000')```


**Challenge 2**

* The **abs** function was used to return the exact value and get rid of the negative value of the trading range

* __Assumptions made__: The trading range was found by using the **'open'** and  **'close'** trades found in the table, sample_dataset3
* To execute stored procedure for sql challenge 2:
```call sp_challenges2();```
