DELIMITER //
create procedure sp_challenge2()
BEGIN


create temporary table tradingrange
select distinct `date`, cast(abs(open-close) as decimal (4,4)) as `range` from sd3
order by 2 desc limit 3;

select * from tradingrange;


create temporary table maximumhigh
select `date`, max(high) as `Maxhigh` from sd3
where `date` in (select `date` from tradingrange)
group by Date;

create temporary table maxtime
select distinct date, time from sd3
group by date
order by 2 desc limit 4;

select `date`, `range` from maxtime
join tradingrange on maxtime.time=tradingrange.date;

end //
DELIMITER ;

call sp_challenges2();
drop procedure sp_challenges2;
